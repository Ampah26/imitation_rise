// server.js
const express = require("express");
const cors = require("cors");
const passport = require("./passport-config");
require("dotenv").config();
const { Pool } = require("pg");

const app = express();

// Middleware setup
app.use(cors());
app.use(express.json());
app.use(passport.initialize());

// Import routes
const clientRoutes = require("./routes/clientRoutes");
const taskRoutes = require("./routes/taskRoutes");
const userRoutes = require("./routes/userRoutes");
const authRoutes = require("./routes/authRoutes");
const dashboardRoutes = require("./routes/dashboardRoutes");
const historyRoutes = require("./routes/historyRoutes");
const projectRoutes = require("./routes/projectRoutes");
const teamRoutes = require("./routes/teamRoutes");
const messageRoutes = require("./routes/messageRoutes");

app.use("/api/clients", clientRoutes);
app.use("/api/tasks", taskRoutes);
app.use("/api/users", userRoutes);
app.use("/api/auth", authRoutes);
app.use("/api/dashboard", dashboardRoutes);
app.use("/api/history", historyRoutes);
app.use("/api/projects", projectRoutes);
app.use("/api/team", teamRoutes);
app.use("/api/messages", messageRoutes);

const PORT = 5000; // Port fixé à 5000

// Lance le serveur seulement si l'environnement n'est pas en mode test

app.listen(PORT, "localhost", () => {
  console.log(`Server started on localhost:${PORT}`);
});
module.exports = app; // Export de l'application pour les tests
