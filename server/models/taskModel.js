// models/taskModel.js
const pool = require("../config/dbConfig");

// Fonction pour obtenir toutes les tâches
const getTasks = async () => {
  const result = await pool.query("SELECT * FROM tasks");
  return result.rows;
};

// models/taskModel.js
const createTask = async (
  title,
  description,
  status,
  priority,
  labels,
  start_date,
  deadline,
  project_id,
  assigned_to,
  collaborator,
  modified_by
) => {
  const result = await pool.query(
    `INSERT INTO tasks (title, description, status, priority, labels, start_date, deadline, project_id, assigned_to, collaborator, modified_by) 
    VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11) RETURNING *`,
    [
      title,
      description,
      status,
      priority,
      labels,
      start_date,
      deadline,
      project_id,
      assigned_to,
      collaborator,
      modified_by, // Ajoutez ce paramètre ici
    ]
  );

  // Enregistrer l'historique de l'insertion
  const newTask = result.rows[0];
  await pool.query(
    `INSERT INTO history (table_name, record_id, operation, new_data, modified_by)
    VALUES ($1, $2, $3, $4, $5)`,
    [
      "tasks",
      newTask.id, // Assurez-vous que l'ID de la nouvelle tâche est utilisé
      "INSERT",
      JSON.stringify(newTask), // Enregistrer les nouvelles données
      modified_by, // Enregistrer qui a effectué l'insertion
    ]
  );

  return newTask; // Retourner la tâche créée
};

// Fonction pour supprimer une tâche par ID
const deleteTask = async (taskId, modified_by) => {
  const taskToDelete = await pool.query("SELECT * FROM tasks WHERE id = $1", [
    taskId,
  ]);

  const result = await pool.query(
    "DELETE FROM tasks WHERE id = $1 RETURNING *",
    [taskId]
  );

  // Enregistrer l'historique de la suppression
  if (result.rowCount > 0) {
    await pool.query(
      `INSERT INTO history (table_name, record_id, operation, old_data, modified_by)
      VALUES ($1, $2, $3, $4, $5)`,
      [
        "tasks",
        taskId,
        "DELETE",
        JSON.stringify(taskToDelete.rows[0]), // Enregistrer les anciennes données
        modified_by, // Enregistrer qui a effectué la suppression
      ]
    );
  }

  return { result, taskToDelete: taskToDelete.rows[0] }; // Retourner le résultat de la suppression et les données de la tâche
};
module.exports = { getTasks, createTask, deleteTask };
