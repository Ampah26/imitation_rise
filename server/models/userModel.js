// models/userModel.js
const pool = require("../config/dbConfig");
const bcrypt = require("bcrypt");

// Fonction pour récupérer tous les utilisateurs
const getAllUsers = async () => {
  const result = await pool.query("SELECT * FROM utilisateur");
  return result.rows;
};

// Créer un nouvel utilisateur
const createUser = async ({ email, nom, prenom, password }) => {
  const saltRounds = 10;
  const hashedPassword = await bcrypt.hash(password, saltRounds);

  const result = await pool.query(
    "INSERT INTO utilisateur (email, nom, prenom, password) VALUES ($1, $2, $3, $4) RETURNING *",
    [email, nom, prenom, hashedPassword]
  );
  return result.rows[0];
};

module.exports = { getAllUsers, createUser };
