// models/projectModel.js
const pool = require("../config/dbConfig");

// Fonction pour obtenir tous les projets
const getProjects = async () => {
  const result = await pool.query("SELECT * FROM projects");
  return result.rows;
};

// Fonction pour créer un nouveau projet
const createProject = async (
  name,
  description,
  client_id,
  start_date,
  end_date,
  status = "pending",
  budget,
  amount_spent,
  modified_by // Ajoutez ce paramètre
) => {
  const clientCheck = await pool.query("SELECT * FROM client WHERE id = $1", [
    client_id,
  ]);

  console.log({
    name,
    description,
    client_id,
    start_date,
    end_date,
    status,
    budget,
    amount_spent,
  });

  const result = await pool.query(
    `INSERT INTO projects (
    name, 
    description,
    client_id,
    start_date,
    end_date,
    status, 
    budget, 
    amount_spent) 
    VALUES ($1, $2, $3, $4, $5, $6, $7, $8) RETURNING *`,
    [
      name,
      description,
      client_id,
      start_date,
      end_date,
      status,
      budget,
      amount_spent,
    ]
  );

  // Enregistrer l'historique de l'insertion
  const newProject = result.rows[0];
  await pool.query(
    `INSERT INTO history (table_name, record_id, operation, new_data, modified_by)
    VALUES ($1, $2, $3, $4, $5)`,
    [
      "projects",
      newProject.id, // Assurez-vous que l'ID du nouveau projet est utilisé
      "INSERT",
      JSON.stringify(newProject), // Enregistrer les nouvelles données
      modified_by, // Enregistrer qui a effectué l'insertion
    ]
  );

  return newProject; // Retourner le projet créé
};

// Fonction pour supprimer un projet
const deleteProject = async (projectId, modified_by) => {
  // Vérifier si le projet existe
  const projectToDelete = await pool.query(
    "SELECT * FROM projects WHERE id = $1",
    [projectId]
  );

  if (projectToDelete.rowCount === 0) {
    console.error("Projet non trouvé pour l'ID :", projectId);
    throw new Error("Le projet spécifié n'existe pas.");
  }

  // Supprimer le projet
  const result = await pool.query(
    "DELETE FROM projects WHERE id = $1 RETURNING *",
    [projectId]
  );

  if (result.rowCount === 0) {
    console.error("Échec de la suppression du projet avec l'ID :", projectId);
    throw new Error("Échec de la suppression du projet.");
  }

  // Enregistrer l'historique de la suppression
  try {
    await pool.query(
      `INSERT INTO history (table_name, record_id, operation, old_data, modified_by)
      VALUES ($1, $2, $3, $4, $5)`,
      [
        "projects", // Nom de la table
        projectId, // ID du projet supprimé
        "DELETE", // Opération de suppression
        JSON.stringify(projectToDelete.rows[0]), // Anciennes données du projet
        modified_by || "Inconnu", // Utilisateur ayant effectué la suppression, valeur par défaut "Inconnu"
      ]
    );
    console.log("Historique de la suppression enregistré avec succès.");
  } catch (error) {
    console.error("Erreur lors de l'insertion dans l'historique :", error);
  }

  return { result, projectToDelete: projectToDelete.rows[0] }; // Retourner le résultat de la suppression et les données du projet
};

// Met à jour un projet
const updateProject = async (projectId, projectData) => {
  const {
    name,
    description,
    client_id,
    start_date,
    end_date,
    status,
    budget,
    amount_spent,
  } = projectData;

  const result = await pool.query(
    `UPDATE project 
     SET name = $1, description = $2, client_id = $3, start_date = $4, 
         end_date = $5, status = $6, budget = $7, amount_spent = $8
     WHERE id = $9 
     RETURNING *`,
    [
      name,
      description,
      client_id,
      start_date,
      end_date,
      status,
      budget,
      amount_spent,
      projectId,
    ]
  );
  return result.rows[0];
};

const getDelayedOrUpcomingProjects = async () => {
  const query = `
    SELECT * FROM projects 
    WHERE end_date < NOW() + INTERVAL '7 days'
  `;
  const result = await pool.query(query);
  return result.rows;
};

module.exports = {
  getProjects,
  createProject,
  deleteProject,
  updateProject,

  getDelayedOrUpcomingProjects,
};
