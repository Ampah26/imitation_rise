// models/authModel.js
const pool = require("../config/dbConfig");
const bcrypt = require("bcrypt");

// Récupérer un utilisateur par email
const getUserByEmail = async (email) => {
  const result = await pool.query(
    "SELECT * FROM utilisateur WHERE email = $1",
    [email]
  );
  return result.rows[0];
};

module.exports = { getUserByEmail };
