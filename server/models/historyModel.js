// models/historyModel.js
const pool = require("../config/dbConfig");

// Fonction pour récupérer l'historique des modifications
const getHistory = async () => {
  const result = await pool.query(
    "SELECT * FROM history ORDER BY modified_at DESC"
  );
  return result.rows;
};

module.exports = { getHistory };
