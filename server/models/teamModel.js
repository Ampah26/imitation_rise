const pool = require("../config/dbConfig");

// Get all team members
const getAllTeamMembers = async () => {
  const result = await pool.query(
    "SELECT equipe.id, utilisateur.nom, utilisateur.prenom, equipe.role FROM equipe " +
      "JOIN utilisateur ON equipe.user_id = utilisateur.id"
  );
  return result.rows;
};

// Add a new team member
const addTeamMember = async (userId, role) => {
  const result = await pool.query(
    "INSERT INTO equipe (user_id, role) VALUES ($1, $2) RETURNING *",
    [userId, role]
  );
  return result.rows[0];
};

// Delete a team member by ID
const deleteTeamMember = async (id) => {
  await pool.query("DELETE FROM equipe WHERE id = $1", [id]);
};

module.exports = {
  getAllTeamMembers,
  addTeamMember,
  deleteTeamMember,
};
