const pool = require("../config/dbConfig");

// Récupère tous les clients
const getAllClients = async () => {
  const result = await pool.query("SELECT * FROM client");
  return result.rows;
};

// Récupère un client par son ID
const getClientById = async (clientId) => {
  const result = await pool.query("SELECT * FROM client WHERE id = $1", [
    clientId,
  ]);
  return result.rows[0];
};

// Crée un nouveau client
const createNewClient = async (clientData) => {
  const {
    name,
    primary_contact,
    phone,
    client_group,
    labels,
    projects,
    total_invoiced,
    payment_received,
    due,
    address,
    city,
    state,
    zip,
    country,
    website,
    vat_number,
    fiscal_identification,
    currency = "EURO",
    currency_symbol = "€",
  } = clientData;

  const result = await pool.query(
    `INSERT INTO client (name, primary_contact, phone, client_group, labels, projects, total_invoiced, 
    payment_received, due, address, city, state, zip, country, website, vat_number, fiscal_identification, 
    currency, currency_symbol) 
     VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19) RETURNING *`,
    [
      name,
      primary_contact,
      phone,
      client_group,
      labels,
      projects,
      total_invoiced,
      payment_received,
      due,
      address,
      city,
      state,
      zip,
      country,
      website,
      vat_number,
      fiscal_identification,
      currency,
      currency_symbol,
    ]
  );
  return result.rows[0];
};

// Met à jour un client
const updateClient = async (clientId, clientData) => {
  const {
    name,
    primary_contact,
    phone,
    client_group,
    labels,
    projects,
    total_invoiced,
    payment_received,
    due,
    address,
    city,
    state,
    zip,
    country,
    website,
    vat_number,
    fiscal_identification,
    currency,
    currency_symbol,
  } = clientData;

  const result = await pool.query(
    `UPDATE client 
     SET name = $1, primary_contact = $2, phone = $3, client_group = $4, labels = $5, 
         projects = $6, total_invoiced = $7, payment_received = $8, due = $9, 
         address = $10, city = $11, state = $12, zip = $13, country = $14, 
         website = $15, vat_number = $16, fiscal_identification = $17, 
         currency = $18, currency_symbol = $19
     WHERE id = $20 
     RETURNING *`,
    [
      name,
      primary_contact,
      phone,
      client_group,
      labels,
      projects,
      total_invoiced,
      payment_received,
      due,
      address,
      city,
      state,
      zip,
      country,
      website,
      vat_number,
      fiscal_identification,
      currency,
      currency_symbol,
      clientId,
    ]
  );
  return result.rows[0];
};

// Supprime un client
const deleteClientById = async (clientId) => {
  const result = await pool.query(
    "DELETE FROM client WHERE id = $1 RETURNING *",
    [clientId]
  );
  return result;
};

module.exports = {
  getAllClients,
  getClientById,
  createNewClient,
  updateClient,
  deleteClientById,
};
