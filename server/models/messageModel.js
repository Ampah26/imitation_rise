const pool = require("../config/dbConfig");

// Récupère tous les messages
const getAllMessages = async () => {
  const result = await pool.query(
    "SELECT * FROM messages ORDER BY created_at ASC"
  );
  return result.rows;
};

// Crée un nouveau message
const createNewMessage = async (messageData) => {
  const { sender_id, sender_name, content } = messageData;

  const result = await pool.query(
    "INSERT INTO messages (sender_id, sender_name, content) VALUES ($1, $2, $3) RETURNING *",
    [sender_id, sender_name, content]
  );
  return result.rows[0];
};

// Supprime un message par son ID
const deleteMessageById = async (messageId) => {
  const result = await pool.query(
    "DELETE FROM messages WHERE id = $1 RETURNING *",
    [messageId]
  );
  return result;
};

module.exports = {
  getAllMessages,
  createNewMessage,
  deleteMessageById,
};
