// models/dashboardModel.js
const pool = require("../config/dbConfig");

// Fonction pour obtenir le nombre total de tâches
const getTotalTasks = async () => {
  const result = await pool.query("SELECT COUNT(*) AS total_tasks FROM tasks");
  return result.rows[0].total_tasks;
};

// Fonction pour obtenir le nombre total de clients
const getTotalClients = async () => {
  const result = await pool.query(
    "SELECT COUNT(*) AS total_clients FROM client"
  );
  return result.rows[0].total_clients;
};

// Fonction pour obtenir le montant total dû
const getTotalDue = async () => {
  const result = await pool.query("SELECT SUM(due) AS total_due FROM client");
  return result.rows[0].total_due;
};

// Fonction pour obtenir le montant total dû
const getTotalBudget = async () => {
  const result = await pool.query(
    "SELECT SUM(budget) AS total_budget FROM projects"
  );
  return result.rows[0].total_budget;
};

// Fonction pour obtenir le nombre total de projets
const getTotalProjects = async () => {
  const result = await pool.query(
    "SELECT COUNT(*) AS total_projects FROM projects"
  );
  return result.rows[0].total_projects;
};

module.exports = {
  getTotalTasks,
  getTotalClients,
  getTotalDue,
  getTotalProjects,
  getTotalBudget,
};
