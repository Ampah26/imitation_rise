const clientModel = require("../models/clientModel");

// Contrôleur pour récupérer tous les clients
exports.getClients = async (req, res) => {
  try {
    const clients = await clientModel.getAllClients();
    res.json(clients);
  } catch (error) {
    console.error("Erreur lors de la récupération des clients:", error);
    res
      .status(500)
      .json({ error: "Erreur lors de la récupération des clients" });
  }
};

// Contrôleur pour récupérer un client par son ID
exports.getClientById = async (req, res) => {
  const clientId = parseInt(req.params.id, 10);
  if (isNaN(clientId)) {
    return res.status(400).json({ message: "ID de client invalide" });
  }

  try {
    const client = await clientModel.getClientById(clientId);
    if (client) {
      res.json(client);
    } else {
      res.status(404).json({ message: "Client non trouvé" });
    }
  } catch (error) {
    console.error("Erreur lors de la récupération du client:", error);
    res.status(500).json({ error: "Erreur lors de la récupération du client" });
  }
};

// Contrôleur pour créer un nouveau client
exports.createClient = async (req, res) => {
  try {
    const client = await clientModel.createNewClient(req.body);
    res.status(201).json({ message: "Client ajouté avec succès", client });
  } catch (error) {
    console.error("Erreur lors de l'ajout du client:", error);
    res.status(500).json({ error: "Erreur lors de l'ajout du client" });
  }
};

// Contrôleur pour mettre à jour un client
exports.updateClient = async (req, res) => {
  const clientId = parseInt(req.params.id, 10);
  if (isNaN(clientId)) {
    return res.status(400).json({ message: "ID de client invalide" });
  }

  try {
    const updatedClient = await clientModel.updateClient(clientId, req.body);
    if (updatedClient) {
      res.json({
        message: "Client modifié avec succès",
        client: updatedClient,
      });
    } else {
      res.status(404).json({ message: "Client non trouvé" });
    }
  } catch (error) {
    console.error("Erreur lors de la modification du client:", error);
    res.status(500).json({ error: "Erreur lors de la modification du client" });
  }
};

// Contrôleur pour supprimer un client
exports.deleteClient = async (req, res) => {
  const clientId = parseInt(req.params.id, 10);
  if (isNaN(clientId)) {
    return res.status(400).json({ message: "ID de client invalide" });
  }

  try {
    const result = await clientModel.deleteClientById(clientId);
    if (result.rowCount > 0) {
      res.status(200).json({ message: "Client supprimé avec succès" });
    } else {
      res.status(404).json({ message: "Client non trouvé" });
    }
  } catch (error) {
    console.error("Erreur lors de la suppression du client:", error);
    res.status(500).json({ error: "Erreur lors de la suppression du client" });
  }
};
