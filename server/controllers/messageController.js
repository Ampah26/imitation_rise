const messageModel = require("../models/messageModel");

// Contrôleur pour récupérer tous les messages
exports.getMessages = async (req, res) => {
  try {
    const messages = await messageModel.getAllMessages();
    res.json(messages);
  } catch (error) {
    console.error("Erreur lors de la récupération des messages:", error);
    res
      .status(500)
      .json({ error: "Erreur lors de la récupération des messages" });
  }
};

// Contrôleur pour créer un nouveau message
exports.createMessage = async (req, res) => {
  try {
    const { sender_id, sender_name, content } = req.body;

    if (!sender_id || !sender_name || !content) {
      return res
        .status(400)
        .json({ message: "Tous les champs sont obligatoires" });
    }

    const newMessage = await messageModel.createNewMessage({
      sender_id,
      sender_name,
      content,
    });
    res.status(201).json({ message: "Message envoyé avec succès", newMessage });
  } catch (error) {
    console.error("Erreur lors de l'envoi du message:", error);
    res.status(500).json({ error: "Erreur lors de l'envoi du message" });
  }
};

// Contrôleur pour supprimer un message
exports.deleteMessage = async (req, res) => {
  const messageId = parseInt(req.params.id, 10);
  if (isNaN(messageId)) {
    return res.status(400).json({ message: "ID de message invalide" });
  }

  try {
    const result = await messageModel.deleteMessageById(messageId);
    if (result.rowCount > 0) {
      res.status(200).json({ message: "Message supprimé avec succès" });
    } else {
      res.status(404).json({ message: "Message non trouvé" });
    }
  } catch (error) {
    console.error("Erreur lors de la suppression du message:", error);
    res.status(500).json({ error: "Erreur lors de la suppression du message" });
  }
};
