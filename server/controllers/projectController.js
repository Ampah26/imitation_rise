// controllers/projectController.js
const projectModel = require("../models/projectModel");

// Récupérer tous les projets
exports.getProjects = async (req, res) => {
  try {
    const projects = await projectModel.getProjects();
    res.json(projects);
  } catch (error) {
    console.error("Error fetching projects:", error);
    res.status(500).json({ error: "Error fetching projects" });
  }
};

// Créer un nouveau projet
exports.createProject = async (req, res) => {
  const {
    name,
    description,
    client_id,
    start_date,
    end_date,
    status,
    budget,
    amount_spent,
  } = req.body;

  // Récupérer le nom et le prénom de l'utilisateur depuis le local storage
  const modified_by = req.headers["x-modified-by"]; // Assurez-vous d'envoyer cela depuis le front-end

  try {
    const project = await projectModel.createProject(
      name,
      description,
      client_id,
      start_date,
      end_date,
      status,
      budget,
      amount_spent,
      modified_by
    );
    res.status(201).json({ message: "Project added successfully", project });
  } catch (error) {
    console.error("Error adding project:", error);
    res.status(500).json({ error: "Error adding project" });
  }
};

// Mettre à jour un projet
exports.updateProject = async (req, res) => {
  const projectId = parseInt(req.params.id, 10);
  if (isNaN(projectId)) {
    return res.status(400).json({ message: "ID de projet invalide" });
  }

  try {
    const updatedProject = await projectModel.updateProject(
      projectId,
      req.body
    );
    if (updatedProject) {
      res.json({
        message: "Projet modifié avec succès",
        project: updatedProject,
      });
    } else {
      res.status(404).json({ message: "Projet non trouvé" });
    }
  } catch (error) {
    console.error("Erreur lors de la modification du projet:", error);
    res.status(500).json({ error: "Erreur lors de la modification du projet" });
  }
};

// Supprimer un projet
exports.deleteProject = async (req, res) => {
  const projectId = parseInt(req.params.id, 10);
  const modified_by = req.body.modified_by; // Récupérer modified_by depuis le body

  if (isNaN(projectId)) {
    return res.status(400).json({ message: "ID de projet invalide" });
  }

  try {
    const { result } = await projectModel.deleteProject(projectId, modified_by);
    if (result.rowCount > 0) {
      res.status(200).json({ message: "Project deleted successfully" });
    } else {
      res.status(404).json({ message: "Project not found" });
    }
  } catch (error) {
    console.error("Error deleting project:", error);
    res.status(500).json({ error: "Error deleting project" });
  }
};

exports.getDelayedOrUpcomingProjects = async (req, res) => {
  try {
    const projects = await projectModel.getDelayedOrUpcomingProjects();
    res.status(200).json(projects);
  } catch (error) {
    console.error("Error fetching delayed or upcoming projects:", error);
    res.status(500).json({
      error: "Erreur lors de la récupération des rappels de projets.",
    });
  }
};
