const authModel = require("../models/authModel");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

const SECRET_KEY = process.env.SECRET_KEY || "test";

// Fonction de login
exports.login = async (req, res) => {
  const { email, password } = req.body;

  // Vérification des champs obligatoires
  if (!email || !password) {
    return res.status(400).json({ message: "Email and password are required" });
  }

  try {
    const user = await authModel.getUserByEmail(email);

    if (!user) {
      return res.status(401).json({ message: "User not found" });
    }

    const match = await bcrypt.compare(password, user.password);
    if (!match) {
      return res.status(401).json({ message: "Invalid password" });
    }

    const token = jwt.sign(
      { id: user.id, nom: user.nom, prenom: user.prenom },
      SECRET_KEY,
      { expiresIn: "1h" }
    );
    res.json({ token, name: user.nom, firstName: user.prenom });
  } catch (error) {
    console.error("Erreur lors de la connexion:", error);
    res.status(500).json({ message: "Erreur du serveur" });
  }
};
