// controllers/dashboardController.js
const dashboardModel = require("../models/dashboardModel");

exports.getDashboardTotals = async (req, res) => {
  try {
    const [totalTasks, totalClients, totalDue, totalProjects, totalBudget] =
      await Promise.all([
        dashboardModel.getTotalTasks(),
        dashboardModel.getTotalClients(),
        dashboardModel.getTotalDue(),
        dashboardModel.getTotalProjects(),
        dashboardModel.getTotalBudget(),
      ]);

    res.json({
      totalTasks,
      totalClients,
      totalDue,
      totalProjects,
      totalBudget,
    });
  } catch (error) {
    console.error(
      "Erreur lors de la récupération des totaux pour le dashboard:",
      error
    );
    res.status(500).json({
      error: "Erreur lors de la récupération des totaux pour le dashboard",
    });
  }
};
