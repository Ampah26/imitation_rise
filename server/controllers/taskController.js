// controllers/taskController.js
const taskModel = require("../models/taskModel");

exports.getTasks = async (req, res) => {
  try {
    const tasks = await taskModel.getTasks();
    res.json(tasks);
  } catch (error) {
    console.error("Error fetching tasks:", error);
    res.status(500).json({ error: "Error fetching tasks" });
  }
};

// controllers/taskController.js
exports.createTask = async (req, res) => {
  const {
    title,
    description,
    status,
    priority,
    labels,
    start_date,
    deadline,
    project_id,
    assigned_to,
    collaborator,
  } = req.body;

  // Récupérer le nom et le prénom de l'utilisateur depuis le local storage
  const modified_by = req.headers["x-modified-by"]; // Assurez-vous d'envoyer cela depuis le front-end

  try {
    const task = await taskModel.createTask(
      title,
      description,
      status,
      priority,
      labels,
      start_date,
      deadline,
      project_id,
      assigned_to,
      collaborator,
      modified_by // Ajoutez ici
    );
    res.status(201).json({ message: "Tâche ajoutée avec succès", task });
  } catch (error) {
    console.error("Error adding task:", error);
    res.status(500).json({ error: "Error adding task" });
  }
};
// taskController.js

exports.deleteTask = async (req, res) => {
  const taskId = parseInt(req.params.id, 10);
  const { modified_by } = req.body; // Récupérer l'utilisateur qui a effectué la suppression

  if (isNaN(taskId)) {
    return res.status(400).json({ message: "Invalid task ID" });
  }

  try {
    const { result } = await taskModel.deleteTask(taskId, modified_by);
    if (result.rowCount > 0) {
      res.status(200).json({ message: "Task deleted successfully" });
    } else {
      res.status(404).json({ message: "Task not found" });
    }
  } catch (error) {
    console.error("Error deleting task:", error);
    res.status(500).json({ error: "Error deleting task" });
  }
};
