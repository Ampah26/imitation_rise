const pool = require("../config/dbConfig");
const historyModel = require("../models/historyModel");

exports.getHistory = async (req, res) => {
  try {
    const history = await historyModel.getHistory();
    res.json(history);
  } catch (error) {
    console.error("Erreur lors de la récupération de l'historique:", error);
    res.status(500).json({
      error: "Erreur lors de la récupération de l'historique",
    });
  }
};
