const teamModel = require("../models/teamModel");

// Get all team members
exports.getAllTeamMembers = async (req, res) => {
  try {
    const team = await teamModel.getAllTeamMembers();
    res.status(200).json(team);
  } catch (error) {
    console.error("Error fetching team members:", error);
    res.status(500).json({ error: "Failed to fetch team members" });
  }
};

// Add a new team member
exports.addTeamMember = async (req, res) => {
  const { userId, role } = req.body;
  try {
    const newMember = await teamModel.addTeamMember(userId, role);
    res.status(201).json(newMember);
  } catch (error) {
    console.error("Error adding team member:", error);
    res.status(500).json({ error: "Failed to add team member" });
  }
};

// Delete a team member by ID
exports.deleteTeamMember = async (req, res) => {
  const { id } = req.params;
  try {
    await teamModel.deleteTeamMember(id);
    res.status(200).json({ message: "Team member deleted successfully" });
  } catch (error) {
    console.error("Error deleting team member:", error);
    res.status(500).json({ error: "Failed to delete team member" });
  }
};
