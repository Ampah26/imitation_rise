const request = require("supertest");
const app = require("../server"); // Importez l'application depuis server.js

describe("Test des routes API du serveur", () => {
  it("GET /api/history - Devrait retourner une liste d'historique avec un statut 200", async () => {
    const response = await request(app).get("/api/history");
    expect(response.status).toBe(200);
    expect(Array.isArray(response.body)).toBe(true); // Vérifie que le corps de la réponse est un tableau
  });

  it("GET /api/users - Devrait retourner une liste d'utilisateurs avec un statut 200", async () => {
    const response = await request(app).get("/api/users");
    expect(response.status).toBe(200);
    expect(Array.isArray(response.body)).toBe(true); // Vérifie que le corps de la réponse est un tableau
  });

  it("POST /api/auth/login - Devrait échouer avec statut 400 si aucune donnée n'est envoyée", async () => {
    const response = await request(app).post("/api/auth/login").send({}); // Envoie un corps de requête vide
    expect(response.status).toBe(400); // Par exemple, 400 si une validation échoue
    expect(response.body).toHaveProperty("message"); // Vérifie la clé "message" au lieu de "error"
  });

  it("GET /api/projects - Devrait retourner une liste de projets avec un statut 200", async () => {
    const response = await request(app).get("/api/projects");
    expect(response.status).toBe(200);
    expect(Array.isArray(response.body)).toBe(true); // Vérifie que le corps de la réponse est un tableau
  });

  it("GET /api/tasks - Devrait retourner une liste de projets avec un statut 200", async () => {
    const response = await request(app).get("/api/tasks");
    expect(response.status).toBe(200);
    expect(Array.isArray(response.body)).toBe(true); // Vérifie que le corps de la réponse est un tableau
  });

  it("GET /api/clients - Devrait retourner une liste de client avec un statut 200", async () => {
    const response = await request(app).get("/api/clients");
    expect(response.status).toBe(200);
    expect(Array.isArray(response.body)).toBe(true); // Vérifie que le corps de la réponse est un tableau
  });

  it("GET /api/dashboard - Devrait retourner un objet avec un statut 200", async () => {
    const response = await request(app).get("/api/dashboard");

    console.log(response.body); // Pour déboguer la réponse

    expect(response.status).toBe(200);

    // Vérifiez que l'objet contient les bonnes propriétés avec des chaînes de caractères
    expect(response.body).toEqual(
      expect.objectContaining({
        totalTasks: expect.any(String), // Vérifie que c'est une chaîne de caractères
        totalClients: expect.any(String),
        totalDue: expect.any(String),
        totalProjects: expect.any(String), // Si vous attendez également ce champ
      })
    );
  });
});
