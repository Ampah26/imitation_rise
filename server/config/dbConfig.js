const { Pool } = require("pg");

// Récupérer les informations de connexion depuis les variables d'environnement
const pool = new Pool({
  user: process.env.DB_USER || "postgres", // Si la variable n'est pas définie, utiliser une valeur par défaut
  host: process.env.DB_HOST || "localhost",
  database: process.env.DB_NAME || "postgres",
  password: process.env.DB_PASSWORD || "komihoratio",
  port: process.env.DB_PORT || 5432,
});

module.exports = pool;
