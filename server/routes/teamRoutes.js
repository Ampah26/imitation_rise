const express = require("express");
const router = express.Router();
const {
  getAllTeamMembers,
  addTeamMember,
  deleteTeamMember,
} = require("../controllers/teamController");

// Get all team members
router.get("/", getAllTeamMembers);

// Add a new team member
router.post("/", addTeamMember);

// Delete a team member by ID
router.delete("/delete/:id", deleteTeamMember);

module.exports = router;
