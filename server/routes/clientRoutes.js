const express = require("express");
const router = express.Router();
const {
  getClients,
  getClientById,
  createClient,
  updateClient,
  deleteClient,
} = require("../controllers/clientController");

// Routes pour les clients
router.get("/", getClients);
router.get("/:id", getClientById);
router.post("/", createClient);
router.put("/edit/:id", updateClient);
router.delete("/delete/:id", deleteClient);

module.exports = router;
