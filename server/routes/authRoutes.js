const express = require("express");
const router = express.Router();
const { login } = require("../controllers/authControllers");

// Route POST pour login
router.post("/login", login);

// Route GET pour tester l'authentification
router.get("/", (req, res) => {
  res.send("Auth endpoint is working!");
});

module.exports = router;
