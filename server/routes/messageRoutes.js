const express = require("express");
const router = express.Router();
const {
  getMessages,
  createMessage,
  deleteMessage,
} = require("../controllers/messageController");

// Routes pour les messages
router.get("/", getMessages); // Récupérer tous les messages
router.post("/", createMessage); // Envoyer un message
router.delete("/delete/:id", deleteMessage); // Supprimer un message

module.exports = router;
