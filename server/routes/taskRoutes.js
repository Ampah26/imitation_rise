const express = require("express");
const router = express.Router();
const {
  getTasks,
  createTask,
  deleteTask,
} = require("../controllers/taskController");

// Get all tasks
router.get("/", getTasks);

// Create a new task
router.post("/", createTask);

// Delete a task by ID
router.delete("/delete/:id", deleteTask);

module.exports = router;
