const express = require("express");
const router = express.Router();
const {
  getProjects,
  createProject,
  updateProject,
  deleteProject,
  getDelayedOrUpcomingProjects,
} = require("../controllers/projectController");

router.get("/", getProjects);
router.post("/", createProject);
router.put("/edit/:id", updateProject);
router.delete("/delete/:id", deleteProject);
router.get("/reminder", getDelayedOrUpcomingProjects);

module.exports = router;
