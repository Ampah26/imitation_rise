const express = require("express");
const router = express.Router();

const { getAllUsers } = require("../controllers/userController");
const { getUtilisateurNames } = require("../controllers/userController");
const { register } = require("../controllers/userController");

// Route pour récupérer tous les utilisateurs
router.get("/", getAllUsers);
// Route POST pour register
router.post("/register", register);

module.exports = router;
