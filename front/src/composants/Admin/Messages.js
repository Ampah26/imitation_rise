import React, { useState, useEffect, useRef } from "react";
import Navbar from "../Navbar";
import Sidebar from "../Sidebar";
import "feather-icons";
import Swal from "sweetalert2";

// Constants
const COLORS = [
  "#FF5733",
  "#33FF57",
  "#5733FF",
  "#FF33A1",
  "#33FFF3",
  "#F39C12",
  "#8E44AD",
  "#2ECC71",
  "#3498DB",
  "#E74C3C",
];

const getSenderColor = (name) =>
  !name ? "#000" : COLORS[name.charCodeAt(0) % COLORS.length];

const Messages = () => {
  // State hooks
  const [messages, setMessages] = useState([]);
  const [newMessage, setNewMessage] = useState("");
  const [replyTo, setReplyTo] = useState(null);

  // Refs
  const messagesEndRef = useRef(null);

  // Fetch messages from backend
  const fetchMessages = async () => {
    try {
      const response = await fetch("http://localhost:5000/api/messages");
      if (!response.ok)
        throw new Error("Erreur lors de la récupération des messages");
      setMessages(await response.json());
    } catch (error) {
      console.error(error);
      Swal.fire("Erreur !", "Impossible de charger les messages.", "error");
    }
  };

  // Send new message
  const handleSendMessage = async () => {
    if (!newMessage.trim()) return;

    try {
      const senderId = localStorage.getItem("userId");
      const senderName = `${localStorage.getItem(
        "prenom"
      )} ${localStorage.getItem("nom")}`;
      if (!senderId) {
        Swal.fire({
          icon: "warning",
          title: "Connexion requise",
          text: "Vous devez être connecté pour envoyer un message.",
          confirmButtonText: "Se connecter",
        }).then((result) => {
          if (result.isConfirmed) window.location.href = "/login";
        });
        return;
      }

      const response = await fetch("http://localhost:5000/api/messages", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          sender_id: senderId,
          sender_name: senderName,
          content: newMessage,
          reply_to: replyTo,
        }),
      });

      if (!response.ok)
        throw new Error(
          (await response.json()).error || "Échec de l'envoi du message"
        );

      setNewMessage("");
      setReplyTo(null);
      await fetchMessages();
    } catch (error) {
      console.error(error);
      Swal.fire(
        "Erreur !",
        error.message || "Impossible d'envoyer le message.",
        "error"
      );
    }
  };

  // Effects
  useEffect(() => {
    fetchMessages();
  }, []);
  useEffect(() => {
    messagesEndRef.current?.scrollIntoView({ behavior: "smooth" });
  }, [messages]);

  return (
    <div>
      <Navbar />
      <div id="left-menu-toggle-mask">
        <Sidebar />
        <div class="page-container overflow-auto">
          <div
            class="main-scrollable-page scrollable-page"
            style={{
              height: "638px",
              position: "relative",
              overflowY: "scroll",
            }}
          >
            <div
              id="page-content"
              class="page-wrapper clearfix"
              style={{ minHeight: "588px" }}
            >
              <div class="clearfix mb15 client-header-area">
                <div class="clearfix float-start">
                  <span class="float-start p10 pl0">
                    <span
                      style={{ backgroundColor: "#fff" }}
                      class="color-tag border-circle"
                    ></span>
                  </span>
                  <h4 class="float-start">Messages</h4>
                </div>
              </div>

              <div className="message-list">
                {messages.length > 0 ? (
                  messages.map((message) => (
                    <div
                      key={message.id}
                      className={`message-item ${
                        message.sender_id ===
                        parseInt(localStorage.getItem("userId"))
                          ? "sent"
                          : "received"
                      }`}
                    >
                      <div
                        className="message-sender"
                        style={{
                          color: getSenderColor(message.sender_name),
                          fontWeight: "bold",
                        }}
                      >
                        {message.sender_name}
                      </div>
                      <div className="message-content">{message.content}</div>
                      {message.reply_to && (
                        <div className="message-reply">
                          <strong>Réponse à :</strong>{" "}
                          {messages.find((m) => m.id === message.reply_to)
                            ?.content || "Message supprimé"}
                        </div>
                      )}
                      <div className="message-time">
                        {new Date(message.created_at).toLocaleTimeString()}
                      </div>
                      <button
                        className="btn btn-reply"
                        onClick={() => setReplyTo(message.id)}
                      >
                        Répondre
                      </button>
                    </div>
                  ))
                ) : (
                  <p>Aucun message à afficher.</p>
                )}
                <div ref={messagesEndRef} />
              </div>
            </div>
          </div>
          <div className="reply-form-container">
            {replyTo && (
              <div className="reply-info">
                <strong>Réponse à :</strong>{" "}
                {messages.find((m) => m.id === replyTo)?.content ||
                  "Message supprimé"}
                <button
                  onClick={() => setReplyTo(null)}
                  className="btn-cancel-reply"
                >
                  ✖ Annuler
                </button>
              </div>
            )}
            <textarea
              name="reply_message"
              rows="4"
              className="form-control"
              placeholder="Écrivez une réponse..."
              value={newMessage}
              onChange={(e) => setNewMessage(e.target.value)}
            />
            <div className="reply-footer">
              <button className="btn-send" onClick={handleSendMessage}>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="24"
                  height="24"
                  viewBox="0 0 24 24"
                  fill="none"
                  stroke="currentColor"
                  strokeWidth="2"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  className="feather feather-send"
                >
                  <line x1="22" y1="2" x2="11" y2="13"></line>
                  <polygon points="22 2 15 22 11 13 2 9 22 2"></polygon>
                </svg>
                Envoyer
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Messages;
