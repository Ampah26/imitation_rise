import React, { useState, useEffect } from "react";
import Navbar from "../Navbar";
import Sidebar from "../Sidebar";
import "feather-icons";

const Dashboard = () => {
  const [totalTasks, setTotalTasks] = useState(0);
  const [totalClients, setTotalClients] = useState(0);
  const [totalDue, setTotalDue] = useState(0);
  const [TotalBudget, setTotalBudget] = useState(0);
  const [modifications, setModifications] = useState([]);
  const [reminders, setReminders] = useState([]); // État pour les rappels
  const [currentPage, setCurrentPage] = useState(0);
  const [totalPages, setTotalPages] = useState(0); // Déclaration de totalPages
  const entriesPerPage = 4;

  const fetchTotals = async () => {
    try {
      const response = await fetch("http://localhost:5000/api/dashboard");
      const data = await response.json();
      setTotalTasks(data.totalTasks);
      setTotalClients(data.totalClients);
      setTotalDue(parseFloat(data.totalDue)); // Convertir en nombre
      setTotalBudget(parseFloat(data.totalBudget)); // Convertir en nombre
    } catch (error) {
      console.error("Erreur lors de la récupération des totaux:", error);
    }
  };

  const fetchHistory = async () => {
    try {
      const response = await fetch("http://localhost:5000/api/history");
      const data = await response.json();
      setModifications(data);
      setTotalPages(Math.ceil(data.length / entriesPerPage)); // Calculer totalPages
    } catch (error) {
      console.error("Erreur lors de la récupération de l'historique:", error);
    }
  };

  // Fonction pour récupérer les rappels
  const fetchReminders = async () => {
    try {
      const response = await fetch(
        "http://localhost:5000/api/projects/reminder"
      );
      const data = await response.json();
      setReminders(data);
    } catch (error) {
      console.error("Erreur lors de la récupération des rappels:", error);
    }
  };

  useEffect(() => {
    fetchTotals();
    fetchHistory();
    fetchReminders(); // Récupération des rappels
  }, []);
  // Calculer les entrées à afficher pour la page actuelle
  const displayedEntries = modifications.slice(
    currentPage * entriesPerPage,
    (currentPage + 1) * entriesPerPage
  );

  // Fonction pour gérer le changement de page
  const handleNextPage = () => {
    if ((currentPage + 1) * entriesPerPage < modifications.length) {
      setCurrentPage(currentPage + 1);
    }
  };

  const handlePreviousPage = () => {
    if (currentPage > 0) {
      setCurrentPage(currentPage - 1);
    }
  };

  return (
    <div>
      <Navbar />
      <div id="left-menu-toggle-mask" style={{ height: "723px" }}>
        <Sidebar />
        <div class="page-container overflow-auto">
          <div
            class="main-scrollable-page scrollable-page"
            style={{
              height: "631px",
              position: "relative",
              overflowY: "scroll",
            }}
          >
            <div
              id="page-content"
              class="page-wrapper clearfix dashboard-view"
              style={{ minHeight: "581px" }}
            >
              <div class="clearfix mb15 dashbaord-header-area">
                <div class="clearfix float-start">
                  <span class="float-start p10 pl0">
                    <span
                      style={{ backgroundColor: "#fff" }}
                      class="color-tag border-circle"
                    ></span>
                  </span>
                  <h4 class="float-start">Dashboard</h4>
                </div>
              </div>
              <div class="clearfix row">
                <div class="col-md-12 widget-container"></div>
              </div>
              <div class="dashboards-row clearfix row">
                <div class="widget-container col-md-3">
                  <div class="card dashboard-icon-widget">
                    <div class="card-body">
                      <div class="widget-icon bg-coral">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="24"
                          height="24"
                          viewBox="0 0 24 24"
                          fill="none"
                          stroke="currentColor"
                          stroke-width="2"
                          stroke-linecap="round"
                          stroke-linejoin="round"
                          class="feather feather-trending-up icon"
                        >
                          <polyline points="23 6 13.5 17.5 8.5 12.5 1 20"></polyline>
                          <circle cx="13.5" cy="17.5" r="1.5"></circle>
                          <circle cx="23" cy="6" r="1.5"></circle>
                        </svg>
                      </div>
                      <div class="widget-details">
                        <h1>€{TotalBudget.toFixed(1).replace(".", ",")}</h1>{" "}
                        {/* Remplacez le point par une virgule */}
                        <span class="bg-transparent-white">Budget Total</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="widget-container col-md-3">
                  <a
                    href="https://rise.fairsketch.com/tasks/all_tasks#my_open_tasks"
                    class="white-link"
                  >
                    <div class="card dashboard-icon-widget">
                      <div class="card-body">
                        <div class="widget-icon bg-info">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="24"
                            height="24"
                            viewBox="0 0 24 24"
                            fill="none"
                            stroke="currentColor"
                            stroke-width="2"
                            stroke-linecap="round"
                            stroke-linejoin="round"
                            class="feather feather-list icon"
                          >
                            <line x1="8" y1="6" x2="21" y2="6"></line>
                            <line x1="8" y1="12" x2="21" y2="12"></line>
                            <line x1="8" y1="18" x2="21" y2="18"></line>
                            <line x1="3" y1="6" x2="3.01" y2="6"></line>
                            <line x1="3" y1="12" x2="3.01" y2="12"></line>
                            <line x1="3" y1="18" x2="3.01" y2="18"></line>
                          </svg>
                        </div>
                        <div class="widget-details">
                          <h1>{totalTasks}</h1>
                          <span class="bg-transparent-white">Total tâches</span>
                        </div>
                      </div>
                    </div>
                  </a>
                </div>
                <div class="widget-container col-md-3">
                  <a
                    href="https://rise.fairsketch.com/events"
                    class="white-link"
                  >
                    <div class="card dashboard-icon-widget">
                      <div class="card-body">
                        <div class="widget-icon bg-success">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="24"
                            height="24"
                            viewBox="0 0 24 24"
                            fill="none"
                            stroke="currentColor"
                            stroke-width="2"
                            stroke-linecap="round"
                            stroke-linejoin="round"
                            class="feather feather-users icon"
                          >
                            <path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
                            <circle cx="9" cy="7" r="4"></circle>
                            <path d="M23 21v-2a4 4 0 0 0-3-3.87"></path>
                            <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>
                          </svg>
                        </div>
                        <div class="widget-details">
                          <h1>{totalClients}</h1>
                          <span class="bg-transparent-white">
                            Total Clients
                          </span>
                        </div>
                      </div>
                    </div>
                  </a>
                </div>
                <div class="widget-container col-md-3">
                  <div class="card  dashboard-icon-widget">
                    <div class="card-body ">
                      <div class="widget-icon bg-coral">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="24"
                          height="24"
                          viewBox="0 0 24 24"
                          fill="none"
                          stroke="currentColor"
                          stroke-width="2"
                          stroke-linecap="round"
                          stroke-linejoin="round"
                          class="feather feather-file-text icon"
                        >
                          <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8l-6-6z"></path>
                          <line x1="16" y1="2" x2="16" y2="8"></line>
                          <line x1="16" y1="13" x2="8" y2="13"></line>
                          <line x1="16" y1="17" x2="8" y2="17"></line>
                        </svg>
                      </div>
                      <div class="widget-details">
                        <h1>€{totalDue.toFixed(1).replace(".", ",")}</h1>{" "}
                        {/* Remplacez le point par une virgule */}
                        <span class="bg-transparent-white">Due</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="dashboards-row clearfix row">
                <div class="col-md-8">
                  <div className="row">
                    <div className="col-md-6">
                      <div class="widget-container">
                        <div class="card bg-white">
                          <div class="card-header">
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              width="24"
                              height="24"
                              viewBox="0 0 24 24"
                              fill="none"
                              stroke="currentColor"
                              stroke-width="2"
                              stroke-linecap="round"
                              stroke-linejoin="round"
                              class="feather feather-users icon-16"
                            >
                              <path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
                              <circle cx="9" cy="7" r="4"></circle>
                              <path d="M23 21v-2a4 4 0 0 0-3-3.87"></path>
                              <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>
                            </svg>
                            &nbsp;Aperçu Utilisateurs
                          </div>
                          <div class="rounded-bottom">
                            <div class="box pt-3">
                              <div class="box-content">
                                <div class="pt-3 pb-3 text-center">
                                  <div class="b-r">
                                    <h3 class="mt-0 strong mb5">5</h3>
                                    <div>Total équipe</div>
                                  </div>
                                </div>
                              </div>
                              <div class="box-content">
                                <div class="p-3 text-center">
                                  <h3 class="mt-0 strong mb5 text-warning">
                                    0
                                  </h3>
                                  <div>Nombre d'utilisateurs</div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div class="widget-container">
                        <div class="card bg-white">
                          <div class="card-header">
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              width="24"
                              height="24"
                              viewBox="0 0 24 24"
                              fill="none"
                              stroke="currentColor"
                              stroke-width="2"
                              stroke-linecap="round"
                              stroke-linejoin="round"
                              class="feather feather-grid icon-16"
                            >
                              <rect x="3" y="3" width="7" height="7"></rect>
                              <rect x="14" y="3" width="7" height="7"></rect>
                              <rect x="14" y="14" width="7" height="7"></rect>
                              <rect x="3" y="14" width="7" height="7"></rect>
                            </svg>{" "}
                            &nbsp;Aperçu Projets
                          </div>
                          <div class="rounded-bottom pt-2">
                            <div class="box">
                              <div class="box-content">
                                <a
                                  href="https://rise.fairsketch.com/projects/all_projects/1"
                                  class="text-default"
                                >
                                  <div class="pt-3 pb10 text-center">
                                    <div class="b-r">
                                      <h4
                                        class="strong mb-1 mt-0"
                                        style={{ color: "#01B393" }}
                                      >
                                        4
                                      </h4>
                                      <span>Crée</span>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="box-content">
                                <a
                                  href="https://rise.fairsketch.com/projects/all_projects/2"
                                  class="text-default"
                                >
                                  <div class="pt-3 pb10 text-center">
                                    <div class="b-r">
                                      <h4 class="strong mb-1 mt-0 text-danger">
                                        7
                                      </h4>
                                      <span>Terminé</span>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="box-content">
                                <a
                                  href="https://rise.fairsketch.com/projects/all_projects/3"
                                  class="text-default"
                                >
                                  <div class="pt-3 pb10 text-center">
                                    <div>
                                      <h4 class="strong mb-1 mt-0 text-warning">
                                        0
                                      </h4>
                                      <span>En cours</span>
                                    </div>
                                  </div>
                                </a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="widget-container">
                    <div className="card bg-white h379">
                      <div class="card-header">
                        <svg
                          width="24"
                          height="24"
                          viewBox="0 0 24 24"
                          fill="none"
                          stroke="currentColor"
                          strokeWidth="2"
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          className="feather feather-bell icon-16"
                        >
                          <path d="M18 8a6 6 0 1 0-12 0c0 7-3 9-3 9h18s-3-2-3-9"></path>
                          <path d="M13.73 21a2 2 0 0 1-3.46 0"></path>
                        </svg>
                        <span> HISTORIQUE </span>
                      </div>
                      <div className="p-3">
                        <div className="history-list">
                          {displayedEntries.length > 0 ? (
                            displayedEntries.map((entry) => (
                              <div
                                key={entry.id}
                                className="history-item border rounded p-3 mb-3"
                                style={{ backgroundColor: "#f9f9f9" }}
                              >
                                <div className="d-flex justify-content-between align-items-center">
                                  <div>
                                    <strong>{entry.full_name}</strong>
                                    <p className="mb-1 text-muted">
                                      {entry.operation} sur la table{" "}
                                      <strong>{entry.table_name}</strong>
                                    </p>
                                    <small className="text-muted">
                                      Enregistrement ID: {entry.record_id}
                                    </small>
                                  </div>
                                  <div>
                                    <small className="text-muted">
                                      {new Date(
                                        entry.modified_at
                                      ).toLocaleString()}
                                    </small>
                                  </div>
                                </div>
                                <div className="mt-2 text-center">
                                  {/* Affichage du nom de l'utilisateur qui a modifié l'enregistrement */}
                                  <strong>
                                    Modifié par : {entry.modified_by}
                                  </strong>
                                </div>
                                <div className="mt-2">
                                  <span
                                    className={`badge ${
                                      entry.operation === "DELETE"
                                        ? "badge-danger"
                                        : entry.operation === "INSERT"
                                        ? "badge-success"
                                        : "badge-secondary"
                                    }`}
                                  >
                                    {entry.operation}
                                  </span>
                                </div>
                              </div>
                            ))
                          ) : (
                            <p>Aucun historique à afficher</p>
                          )}
                        </div>
                        <div className="d-flex justify-content-between align-items-center mt-3">
                          <button
                            onClick={handlePreviousPage}
                            disabled={currentPage === 0}
                            className="btn btn-secondary"
                          >
                            <span>&laquo;</span> {/* Flèche gauche */}
                          </button>
                          <span>
                            Page {currentPage + 1} sur {totalPages}
                          </span>
                          <button
                            onClick={handleNextPage}
                            disabled={currentPage + 1 >= totalPages}
                            className="btn btn-secondary"
                          >
                            <span>&raquo;</span> {/* Flèche droite */}
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* Colonne pour les aperçus */}
                </div>
                <div class="col-md-4">
                  {" "}
                  {/* Colonne pour l'historique */}
                  <div class="widget-container">
                    <div class="card bg-white">
                      <div class="card-header">
                        <svg
                          width="24"
                          height="24"
                          viewBox="0 0 24 24"
                          fill="none"
                          stroke="currentColor"
                          strokeWidth="2"
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          className="feather feather-bell icon-16"
                        >
                          <path d="M18 8a6 6 0 1 0-12 0c0 7-3 9-3 9h18s-3-2-3-9"></path>
                          <path d="M13.73 21a2 2 0 0 1-3.46 0"></path>
                        </svg>
                        <span> Rappels </span>
                      </div>
                      <div className="row p-3">
                        <div className="col-md-12">
                          <div
                            className="reminder-list"
                            style={{ maxHeight: "600px", overflowY: "auto" }}
                          >
                            {reminders.length > 0 ? (
                              reminders.map((reminder, index) => (
                                <div
                                  key={index}
                                  className="reminder-item border rounded p-3 mb-2"
                                  style={{
                                    backgroundColor: "#f9f9f9",
                                    boxShadow: "0 2px 8px rgba(0, 0, 0, 0.1)",
                                  }}
                                >
                                  <div className="d-flex justify-content-between align-items-center">
                                    <div>
                                      <h5
                                        className="font-weight-bold"
                                        style={{ color: "#333" }}
                                      >
                                        {reminder.name}
                                      </h5>
                                      <p
                                        className="mb-1 text-muted"
                                        style={{ fontSize: "14px" }}
                                      >
                                        {reminder.description}
                                      </p>
                                      <div className="d-flex align-items-center">
                                        <svg
                                          xmlns="http://www.w3.org/2000/svg"
                                          width="16"
                                          height="16"
                                          viewBox="0 0 24 24"
                                          fill="none"
                                          stroke="currentColor"
                                          strokeWidth="2"
                                          strokeLinecap="round"
                                          strokeLinejoin="round"
                                          className="feather feather-calendar"
                                          style={{
                                            marginRight: "5px",
                                            color: "#007bff",
                                          }}
                                        >
                                          <rect
                                            x="3"
                                            y="4"
                                            width="18"
                                            height="18"
                                            rx="2"
                                            ry="2"
                                          ></rect>
                                          <line
                                            x1="16"
                                            y1="2"
                                            x2="16"
                                            y2="6"
                                          ></line>
                                          <line
                                            x1="8"
                                            y1="2"
                                            x2="8"
                                            y2="6"
                                          ></line>
                                        </svg>
                                        <small className="text-muted">
                                          Échéance:{" "}
                                          {new Date(
                                            reminder.end_date
                                          ).toLocaleDateString()}
                                        </small>
                                      </div>
                                    </div>
                                    <div>
                                      <span className="badge badge-primary">
                                        Nouvelle
                                      </span>
                                      {/* Badge pour indiquer le statut */}
                                    </div>
                                  </div>
                                </div>
                              ))
                            ) : (
                              <li>Aucun rappel pour le moment</li>
                            )}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <span
        role="status"
        aria-live="polite"
        class="select2-hidden-accessible"
      ></span>
    </div>
  );
};

export default Dashboard;
