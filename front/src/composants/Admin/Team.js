import React, { useState, useEffect } from "react";
import Navbar from "../Navbar";
import Sidebar from "../Sidebar";
import Swal from "sweetalert2";
import AddTeamMemberModal from "../Modal/AddTeamMemberModal";

const Team = () => {
  const [teamMembers, setTeamMembers] = useState([]); // State for team members
  const [showAddModal, setShowAddModal] = useState(false); // State for modal visibility
  const [searchTerm, setSearchTerm] = useState("");

  // Filtrer les membres de l'équipe en fonction du terme de recherche
  const filteredTeam = teamMembers.filter(
    (member) =>
      member.nom.toLowerCase().includes(searchTerm.toLowerCase()) ||
      member.prenom.toLowerCase().includes(searchTerm.toLowerCase())
  );
  // Fetch team members from the API
  const fetchTeamMembers = async () => {
    try {
      const response = await fetch("http://localhost:5000/api/team");
      if (!response.ok) throw new Error("Failed to fetch team members.");
      const data = await response.json();
      setTeamMembers(data);
    } catch (error) {
      console.error("Error fetching team members:", error);
    }
  };

  useEffect(() => {
    fetchTeamMembers();
  }, []);

  // Delete a team member
  const handleDelete = async (id) => {
    const result = await Swal.fire({
      title: "Vous êtes sûr ?",
      text: "Vous ne pourrez pas revenir en arrière !",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "Oui !",
      cancelButtonText: "Annuler",
    });

    if (result.isConfirmed) {
      try {
        // Trouver l'utilisateur à supprimer par ID
        const memberToDelete = teamMembers.find((member) => member.id === id);

        if (!memberToDelete) {
          throw new Error("Membre introuvable ou déjà supprimé");
        }

        // Effectuer la requête DELETE pour supprimer le membre
        const response = await fetch(
          `http://localhost:5000/api/team/delete/${id}`,
          {
            method: "DELETE",
          }
        );

        if (!response.ok) {
          throw new Error("Échec de la suppression du membre d'équipe");
        }

        // Mettre à jour l'état de l'équipe après la suppression
        setTeamMembers(teamMembers.filter((member) => member.id !== id));

        // Afficher le SweetAlert avec le nom et prénom du membre supprimé
        Swal.fire({
          title: "Supprimé !",
          text: `Le membre ${memberToDelete.nom} ${memberToDelete.prenom} a été supprimé.`,
          icon: "success",
          confirmButtonText: "OK",
        });
      } catch (error) {
        console.error("Erreur lors de la suppression du membre :", error);
        Swal.fire(
          "Erreur !",
          error.message || "Échec de la suppression du membre d'équipe.",
          "error"
        );
      }
    }
  };

  return (
    <div>
      <Navbar />
      <div id="left-menu-toggle-mask">
        <Sidebar />
        <div className="page-container">
          <div className="main-scrollable-page">
            <div className="page-wrapper">
              <div className="team-header">
                {/* Header Area */}
                <div className="clearfix mb15 client-header-area">
                  <div className="clearfix float-start">
                    <span className="float-start p10 pl0">
                      <span
                        style={{ backgroundColor: "#fff" }}
                        className="color-tag border-circle"
                      ></span>
                    </span>
                    <h4 className="float-start">Equipes</h4>
                  </div>
                </div>

                {/* Section de recherche */}
                <div className="filter-section-right">
                  <div className="filter-item-box">
                    <div id="client-table_filter" className="dataTables_filter">
                      <label>
                        <input
                          type="search"
                          className="form-control form-control-sm"
                          placeholder="Rechercher un membre..."
                          aria-controls="client-table"
                          value={searchTerm}
                          onChange={(e) => setSearchTerm(e.target.value)}
                        />
                      </label>
                    </div>
                  </div>
                </div>
                {/* Create New Member Button */}
                <button
                  className="btn btn-primary"
                  onClick={() => setShowAddModal(true)}
                >
                  Nouveau membre
                </button>
              </div>

              {/* Team Grid */}
              <div className="team-grid">
                {teamMembers.map((member) => (
                  <div key={member.id} className="team-card">
                    <div className="card-content">
                      <h5>
                        {member.nom} {member.prenom}
                      </h5>
                      <p
                        className={`role ${
                          member.role.toLowerCase().replace(" ", "") ||
                          "default"
                        }`}
                      >
                        {member.role}
                      </p>
                      <div className="card-actions">
                        <button
                          className="btn btn-edit"
                          onClick={() => alert("Edit functionality here")}
                        >
                          Edit
                        </button>
                        <button
                          className="btn btn-delete"
                          onClick={() => handleDelete(member.id)}
                        >
                          Delete
                        </button>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* Add Team Member Modal */}
      <AddTeamMemberModal
        show={showAddModal}
        handleClose={() => setShowAddModal(false)}
        refreshTeam={fetchTeamMembers} // Refresh team list after adding a member
      />
    </div>
  );
};

export default Team;
