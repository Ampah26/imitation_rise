import React, { useState, useEffect } from "react";
import Navbar from "../Navbar";
import Sidebar from "../Sidebar";
import "feather-icons";
import AddProjectModal from "../Modal/AddProjectModal"; // Assurez-vous d'avoir un modal pour ajouter un projet
import Swal from "sweetalert2"; // Assurez-vous d'importer SweetAlert2 pour les alertes
import EditProjectModal from "../Modal/EditProjectModal";

const Projects = () => {
  const [showModal, setShowModal] = useState(false);
  const [showEditModal, setShowEditModal] = useState(false); // État pour le modal d'édition
  const [projects, setProjects] = useState([]);
  const [clients, setClients] = useState([]);
  const [selectedProject, setSelectedProject] = useState(null); // Projet sélectionné pour l'édition

  const handleShowModal = () => setShowModal(true);
  const handleCloseModal = () => setShowModal(false);

  const handleCloseEditModal = () => {
    setShowEditModal(false);
    setSelectedProject(null); // Réinitialiser le projet sélectionné
  };

  const getClientName = (clientId) => {
    const client = clients.find((c) => c.id === clientId);
    return client ? client.name : "Inconnu"; // Utilisez client.name
  };

  const handleEditClick = (project) => {
    setSelectedProject(project);
    setShowEditModal(true);
  };

  const handleDelete = async (projectId) => {
    if (!projectId) {
      console.error("ID de projet invalide :", projectId);
      return;
    }

    const result = await Swal.fire({
      title: "Êtes-vous sûr ?",
      text: "Vous ne pourrez pas revenir en arrière !",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Oui, supprimer !",
      cancelButtonText: "Annuler",
    });

    if (result.isConfirmed) {
      try {
        // Trouver le projet à supprimer par ID
        const projectToDelete = projects.find(
          (project) => project.id === projectId
        );

        if (!projectToDelete) {
          throw new Error("Projet introuvable");
        }

        const nom = localStorage.getItem("nom");
        const prenom = localStorage.getItem("prenom");
        const modifiedBy = nom && prenom ? `${prenom} ${nom}` : "Inconnu";

        // Effectuer la requête DELETE pour supprimer le projet
        const response = await fetch(
          `http://localhost:5000/api/projects/delete/${projectId}`,
          {
            method: "DELETE",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({ modified_by: modifiedBy }), // Assurez-vous d'envoyer `modified_by` ici
          }
        );

        if (!response.ok) {
          throw new Error("Échec de la suppression du projet");
        }

        // Mettre à jour l'état pour supprimer le projet de la liste
        setProjects(projects.filter((project) => project.id !== projectId));

        // Afficher le SweetAlert avec le nom du projet supprimé
        Swal.fire({
          title: "Supprimé !",
          text: `Le projet "${projectToDelete.name}" a été supprimé.`,
          icon: "success",
          confirmButtonText: "OK",
        });
      } catch (error) {
        console.error("Erreur lors de la suppression du projet :", error);
        Swal.fire(
          "Erreur !",
          error.message || "Erreur lors de la suppression du projet",
          "error"
        );
      }
    }
  };

  useEffect(() => {
    // Fonction pour récupérer les données des projets depuis le backend
    const fetchProjects = async () => {
      try {
        const response = await fetch("http://localhost:5000/api/projects");
        const data = await response.json();
        setProjects(data);
        const clientResponse = await fetch("http://localhost:5000/api/clients");
        const clientsData = await clientResponse.json();
        setClients(clientsData);
      } catch (error) {
        console.error("Erreur lors de la récupération des projets:", error);
      }
    };

    fetchProjects();
    const intervalId = setInterval(fetchProjects, 1000); // Rafraîchissement chaque seconde

    // Nettoyer l'intervalle lorsque le composant est démonté
    return () => clearInterval(intervalId);
  }, []);

  return (
    <div>
      <Navbar />
      <div id="left-menu-toggle-mask">
        <Sidebar />
        <div className="page-container overflow-auto">
          <div
            className="main-scrollable-page scrollable-page"
            style={{
              height: "638px",
              position: "relative",
              overflowY: "scroll",
            }}
          >
            <div
              id="page-content"
              className="page-wrapper clearfix"
              style={{ minHeight: "588px" }}
            >
              <div className="clearfix mb15 project-header-area">
                <div className="clearfix float-start">
                  <div class="clearfix mb15 client-header-area">
                    <div class="clearfix float-start">
                      <span class="float-start p10 pl0">
                        <span
                          style={{ backgroundColor: "#fff" }}
                          class="color-tag border-circle"
                        ></span>
                      </span>
                      <h4 class="float-start">Projets</h4>
                    </div>
                  </div>{" "}
                </div>
              </div>
              <div className="clearfix grid-button">
                <div className="title-button-group">
                  <button
                    className="btn btn-default"
                    title="Add project"
                    onClick={handleShowModal}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      stroke="currentColor"
                      strokeWidth="2"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      className="feather feather-plus-circle icon-16"
                    >
                      <circle cx="12" cy="12" r="10"></circle>
                      <line x1="12" y1="8" x2="12" y2="16"></line>
                      <line x1="8" y1="12" x2="16" y2="12"></line>
                    </svg>
                    Nouveau projet
                  </button>
                  <AddProjectModal
                    show={showModal}
                    handleClose={handleCloseModal}
                  />
                </div>
              </div>
              <div className="card">
                <div className="table-responsive">
                  <table
                    id="project-table"
                    className="display dataTable no-footer"
                    cellSpacing="0"
                    width="100%"
                    role="grid"
                  >
                    <thead>
                      <tr role="row" className="table-header">
                        <th className="text-center w50 all sorting bold-text">
                          ID
                        </th>
                        <th className="all sorting bold-text">Nom du Projet</th>
                        <th className="sorting bold-text">Description</th>
                        <th className="sorting bold-text">Client ID</th>
                        <th className="sorting bold-text">Date de Début</th>
                        <th className="sorting bold-text">Date de Fin</th>
                        <th className="sorting bold-text">Statut</th>
                        <th className="sorting bold-text">Budget</th>
                        <th className="sorting bold-text">Montant Dépensé</th>
                        <th className="text-center option w100 sorting bold-text">
                          Actions
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      {projects.map((project, index) => (
                        <tr
                          key={project.id}
                          className={index % 2 === 0 ? "even" : "odd"}
                        >
                          <td className="text-center w50 all">{project.id}</td>
                          <td className="all">{project.name}</td>
                          <td>{project.description}</td>
                          <td>{getClientName(project.client_id)}</td>{" "}
                          {/* Remplacez l'ID par le nom complet */}
                          <td className="date-start">
                            {new Date(project.start_date).toLocaleDateString()}
                          </td>{" "}
                          {/* Classe ajoutée */}
                          <td className="date-end">
                            {new Date(project.end_date).toLocaleDateString()}
                          </td>{" "}
                          {/* Classe ajoutée */}
                          <td>{project.status.trim()}</td>
                          <td>{project.budget}</td>
                          <td>{project.amount_spent}</td>
                          <td className="text-center option w100">
                            <button
                              className="edit"
                              title="Modifier le projet"
                              onClick={() => handleEditClick(project)}
                            >
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="24"
                                height="24"
                                viewBox="0 0 24 24"
                                fill="none"
                                stroke="currentColor"
                                strokeWidth="2"
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                className="feather feather-edit icon-16"
                                onClick={() => handleEditClick(project)}
                              >
                                <path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path>
                                <path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path>
                              </svg>
                            </button>
                            <button
                              title="Supprimer le projet"
                              className="delete"
                              style={{ cursor: "pointer" }}
                              onClick={() => handleDelete(project.id)} // Passer le bon ID de projet
                            >
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="24"
                                height="24"
                                viewBox="0 0 24 24"
                                fill="none"
                                stroke="currentColor"
                                strokeWidth="2"
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                className="feather feather-x icon-16"
                              >
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                              </svg>
                            </button>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <EditProjectModal
        show={showEditModal}
        handleClose={handleCloseEditModal}
        project={selectedProject}
      />
    </div>
  );
};

export default Projects;
