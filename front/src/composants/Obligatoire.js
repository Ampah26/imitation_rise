// Obligatoire.js
import React from "react";

const Obligatoire = () => {
  return (
    <span style={{ color: "#FF7F7F", marginLeft: "5px", fontSize: "0.8em" }}>
      * Obligatoire
    </span>
  );
};

export default Obligatoire;
