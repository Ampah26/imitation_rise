import React, { useState, useEffect } from "react";
import { Modal } from "react-bootstrap";
import Swal from "sweetalert2";
import Obligatoire from "../Obligatoire";

const AddTeamMemberModal = ({ show, handleClose, refreshTeam }) => {
  const [teamMemberData, setTeamMemberData] = useState({
    userId: "", // S'assurer que userId est bien mis à jour
    role: "",
  });
  const [users, setUsers] = useState([]);

  useEffect(() => {
    const fetchUsers = async () => {
      try {
        const response = await fetch("http://localhost:5000/api/users");
        const data = await response.json();
        setUsers(data);
      } catch (error) {
        console.error(
          "Erreur lors de la récupération des utilisateurs:",
          error
        );
      }
    };
    fetchUsers();
  }, []);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setTeamMemberData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    // Vérifiez ici les données envoyées
    console.log("Team Member Data:", teamMemberData);

    try {
      const response = await fetch("http://localhost:5000/api/team", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(teamMemberData),
      });

      if (response.ok) {
        Swal.fire({
          title: "Succès",
          text: "Nouveau membre de l'équipe ajouté avec succès !",
          icon: "success",
          confirmButtonText: "OK",
        });
        handleClose();
        refreshTeam();
      } else {
        Swal.fire({
          title: "Erreur",
          text: "Erreur lors de l'ajout du membre de l'équipe.",
          icon: "error",
          confirmButtonText: "OK",
        });
      }
    } catch (error) {
      console.error("Erreur lors de l'ajout du membre de l'équipe:", error);
      Swal.fire({
        title: "Erreur",
        text: "Erreur lors de la soumission du formulaire.",
        icon: "error",
        confirmButtonText: "OK",
      });
    }
  };

  return (
    <Modal show={show} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>Ajouter un Membre d'Équipe</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <form
          id="add-team-member-form"
          className="general-form"
          acceptCharset="utf-8"
          onSubmit={handleSubmit}
        >
          {/* User ID */}
          <div className="form-group">
            <label htmlFor="user_id">Utilisateur</label>
            <select
              name="userId"
              id="userId"
              className="form-control p10"
              value={teamMemberData.userId}
              onChange={handleChange}
              required
            >
              <option value="" disabled>
                Sélectionnez un utilisateur
              </option>
              {users.map((user) => (
                <option key={user.id} value={user.id}>
                  {user.nom} {user.prenom}
                </option>
              ))}
            </select>
            <Obligatoire />
          </div>

          {/* Role */}
          <div className="form-group">
            <label htmlFor="role">Rôle</label>
            <select
              name="role"
              id="role"
              className="form-control p10"
              value={teamMemberData.role}
              onChange={handleChange}
              required
            >
              <option value="" disabled>
                Sélectionnez un rôle
              </option>
              <option value="frontend">Développeur Front-End</option>
              <option value="backend">Développeur Back-End</option>
              <option value="manager">Chef de Projet</option>
              <option value="designer">Designer</option>
              <option value="qa">Testeur QA</option>
            </select>
            <Obligatoire />
          </div>

          {/* Buttons */}
          <div className="form-group d-flex justify-content-between">
            <button
              type="button"
              className="btn btn-secondary"
              onClick={handleClose}
            >
              Annuler
            </button>
            <button type="submit" className="btn btn-primary ms-auto">
              Valider
            </button>
          </div>
        </form>
      </Modal.Body>
    </Modal>
  );
};

export default AddTeamMemberModal;
