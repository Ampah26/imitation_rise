import React, { useState, useEffect } from "react";
import { Modal } from "react-bootstrap";
import Swal from "sweetalert2";
import DatePicker from "react-datepicker"; // Importer le composant DatePicker
import Obligatoire from "../Obligatoire";

const AddProjectModal = ({ show, handleClose }) => {
  const [projectData, setProjectData] = useState({
    name: "",
    description: "",
    client_id: "", // Assurez-vous de gérer les clients si nécessaire
    start_date: null,
    end_date: null,
    budget: "",
    status: "pending", // Statut par défaut
    amount_spent: 0, // Valeur par défaut, ou vous pouvez le rendre dynamique
  });

  const [clients, setClients] = useState([]); // État pour stocker les clients
  const [dateError, setDateError] = useState(""); // État pour gérer les erreurs de date

  useEffect(() => {
    const fetchClients = async () => {
      try {
        const response = await fetch("http://localhost:5000/api/clients");
        const data = await response.json();
        setClients(data); // Mettre à jour l'état avec les clients récupérés
      } catch (error) {
        console.error("Erreur lors de la récupération des clients:", error);
      }
    };

    fetchClients();
  }, []);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setProjectData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleDateChange = (date, field) => {
    setProjectData((prevData) => ({
      ...prevData,
      [field]: date,
    }));

    if (field === "start_date" && date > projectData.end_date) {
      setDateError("La date de début ne peut pas être après la date de fin.");
    } else if (field === "end_date" && date < projectData.start_date) {
      setDateError("La date de fin doit être après la date de début.");
    } else {
      setDateError("");
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (!projectData.name || !projectData.client_id) {
      Swal.fire({
        title: "Erreur",
        text: "Le nom et le client sont requis.",
        icon: "error",
        confirmButtonText: "OK",
      });
      return; // Ne pas continuer si les validations échouent
    }

    // Récupérer le nom et le prénom de l'utilisateur depuis le local storage
    const nom = localStorage.getItem("nom");
    const prenom = localStorage.getItem("prenom");
    const modifiedBy = nom && prenom ? `${prenom} ${nom}` : "Inconnu"; // Gérer le cas où l'utilisateur n'est pas trouvé

    console.log("Formulaire soumis avec les données:", projectData);

    try {
      const response = await fetch("http://localhost:5000/api/projects", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "x-modified-by": modifiedBy, // Envoyer le nom complet dans l'en-tête
        },
        body: JSON.stringify({
          name: projectData.name,
          description: projectData.description,
          client_id: projectData.client_id,
          start_date: projectData.start_date,
          end_date: projectData.end_date,
          status: projectData.status,
          budget: projectData.budget,
          amount_spent: projectData.amount_spent,
        }),
      });

      if (response.ok) {
        const data = await response.json();
        console.log("Projet ajouté avec succès:", data);
        Swal.fire({
          title: "Succès",
          text: "Nouveau projet ajouté avec succès !",
          icon: "success",
          confirmButtonText: "OK",
        });
        handleClose(); // Fermer le modal après soumission
      } else {
        console.error("Erreur lors de l'ajout du projet");
        Swal.fire({
          title: "Erreur",
          text: "Erreur lors de l'ajout du projet",
          icon: "error",
          confirmButtonText: "OK",
        });
      }
    } catch (error) {
      console.error("Erreur lors de la soumission du formulaire:", error);
      Swal.fire({
        title: "Erreur",
        text: "Erreur lors de la soumission du formulaire",
        icon: "error",
        confirmButtonText: "OK",
      });
    }
  };

  return (
    <Modal show={show} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>Ajouter un Projet</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <form
          id="add-project-form"
          className="general-form"
          acceptCharset="utf-8"
          onSubmit={handleSubmit}
        >
          {/* Nom du Projet */}
          <div className="form-group">
            <label htmlFor="name">Nom du Projet</label>
            <input
              type="text"
              name="name"
              id="name"
              className="form-control p10"
              placeholder="Nom du Projet"
              value={projectData.name}
              onChange={handleChange}
              required
            />
            <Obligatoire />
          </div>

          {/* Description du Projet */}
          <div className="form-group">
            <label htmlFor="description">Description</label>
            <textarea
              name="description"
              id="description"
              className="form-control p10"
              placeholder="Description du Projet"
              value={projectData.description}
              onChange={handleChange}
              rows="3"
            />
          </div>

          {/* Client */}
          <div className="form-group">
            <label htmlFor="client_id">Client</label>
            <select
              name="client_id"
              id="client_id"
              className="form-control p10"
              value={projectData.client_id}
              onChange={handleChange}
              required
            >
              <option value="" disabled>
                Sélectionnez un client
              </option>
              {clients.map((client) => (
                <option key={client.id} value={client.id}>
                  {client.name}
                </option>
              ))}
            </select>
            <Obligatoire />
          </div>

          {/* Budget */}
          <div className="form-group">
            <label htmlFor="budget">Budget</label>
            <input
              type="number"
              name="budget"
              id="budget"
              className="form-control p10"
              placeholder="Budget"
              value={projectData.budget}
              onChange={handleChange}
              required
            />
            <Obligatoire />
          </div>

          {/* Statut */}
          <div className="form-group">
            <label htmlFor="status">Statut</label>
            <select
              name="status"
              id="status"
              className="form-control p10"
              value={projectData.status}
              onChange={handleChange}
            >
              <option value="pending">En attente</option>
              <option value="in_progress">En cours</option>
              <option value="completed">Terminé</option>
            </select>
          </div>

          {/* Dates */}
          <div className="form-group mb-3 d-flex justify-content-between">
            <div className="w-48 me-2">
              <label htmlFor="start_date" className="form-label">
                Date de début
              </label>
              <DatePicker
                selected={projectData.start_date}
                onChange={(date) => handleDateChange(date, "start_date")}
                className="form-control"
                dateFormat="yyyy-MM-dd"
                required
              />
            </div>
            <div className="w-48 ms-2">
              <label htmlFor="end_date" className="form-label">
                Date limite
              </label>
              <DatePicker
                selected={projectData.end_date}
                onChange={(date) => handleDateChange(date, "end_date")}
                className="form-control"
                dateFormat="yyyy-MM-dd"
                required
              />
            </div>
          </div>

          {dateError && <div className="text-danger">{dateError}</div>}

          {/* Boutons */}
          <div className="form-group d-flex justify-content-between">
            <button
              type="button"
              className="btn btn-secondary"
              onClick={handleClose}
            >
              Annuler
            </button>
            <button type="submit" className="btn btn-primary ms-auto">
              Valider
            </button>
          </div>
        </form>
      </Modal.Body>
    </Modal>
  );
};

export default AddProjectModal; // Assurez-vous d'utiliser l'export par défaut
