import React, { useState, useEffect } from "react";
import { Modal } from "react-bootstrap";
import Swal from "sweetalert2";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

const EditProjectModal = ({ show, handleClose, project }) => {
  const [projectData, setProjectData] = useState({
    name: "",
    description: "",
    client_id: "",
    start_date: null,
    end_date: null,
    budget: "",
    status: "",
    amount_spent: "",
  });
  const [clients, setClients] = useState([]); // Liste des clients
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);
  const [dateError, setDateError] = useState("");

  useEffect(() => {
    if (project) {
      setProjectData({
        ...project,
        start_date: new Date(project.start_date),
        end_date: new Date(project.end_date),
      });
      setStartDate(new Date(project.start_date));
      setEndDate(new Date(project.end_date));
    }
  }, [project]);

  useEffect(() => {
    const fetchClients = async () => {
      try {
        const response = await fetch("http://localhost:5000/api/clients");
        const data = await response.json();
        setClients(data);
      } catch (error) {
        console.error("Erreur lors de la récupération des clients :", error);
      }
    };

    fetchClients();
  }, []);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setProjectData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleDateChange = (field, date) => {
    setProjectData((prevData) => ({
      ...prevData,
      [field]: date.toISOString().split("T")[0], // Convertir en format YYYY-MM-DD
    }));
    if (field === "start_date") setStartDate(date);
    if (field === "end_date") setEndDate(date);

    if (field === "start_date" && date > endDate) {
      setDateError("La date de début ne peut pas être après la date de fin.");
    } else if (field === "end_date" && date < startDate) {
      setDateError("La date de fin doit être après la date de début.");
    } else {
      setDateError("");
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const response = await fetch(
        `http://localhost:5000/api/projects/edit/${projectData.id}`,
        {
          method: "PUT",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify({
            ...projectData,
            start_date: startDate.toISOString().split("T")[0],
            end_date: endDate.toISOString().split("T")[0],
          }),
        }
      );

      if (response.ok) {
        Swal.fire("Succès", "Projet modifié avec succès !", "success");
        handleClose();
      } else {
        Swal.fire("Erreur", "Impossible de modifier le projet.", "error");
      }
    } catch (error) {
      console.error("Erreur :", error);
      Swal.fire("Erreur", "Une erreur est survenue.", "error");
    }
  };

  return (
    <Modal show={show} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>Modifier le Projet</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <form onSubmit={handleSubmit} className="general-form">
          <div className="form-group">
            <label htmlFor="name">Nom du Projet</label>
            <input
              type="text"
              name="name"
              className="form-control"
              value={projectData.name || ""}
              onChange={handleChange}
              required
            />
          </div>
          <div className="form-group">
            <label htmlFor="description">Description</label>
            <textarea
              name="description"
              className="form-control"
              value={projectData.description || ""}
              onChange={handleChange}
            />
          </div>
          <div className="form-group">
            <label htmlFor="client_id">Client</label>
            <select
              name="client_id"
              className="form-control"
              value={projectData.client_id || ""}
              onChange={handleChange}
              required
            >
              <option value="" disabled>
                Sélectionnez un client
              </option>
              {clients.map((client) => (
                <option key={client.id} value={client.id}>
                  {client.name}
                </option>
              ))}
            </select>
          </div>
          <div className="form-group mb-3 d-flex justify-content-between">
            <div>
              <label>Date de Début</label>
              <DatePicker
                selected={startDate}
                onChange={(date) => handleDateChange("start_date", date)}
                className="form-control"
                dateFormat="yyyy-MM-dd"
                required
              />
            </div>
            <div>
              <label>Date de Fin</label>
              <DatePicker
                selected={endDate}
                onChange={(date) => handleDateChange("end_date", date)}
                className="form-control"
                dateFormat="yyyy-MM-dd"
                required
              />
            </div>
          </div>
          {dateError && <div className="text-danger">{dateError}</div>}
          <div className="form-group">
            <label>Statut</label>
            <select
              name="status"
              className="form-control"
              value={projectData.status || ""}
              onChange={handleChange}
              required
            >
              <option value="pending">En attente</option>
              <option value="in_progress">En cours</option>
              <option value="completed">Terminé</option>
            </select>
          </div>
          <div className="form-group">
            <label>Budget</label>
            <input
              type="number"
              name="budget"
              className="form-control"
              value={projectData.budget || ""}
              onChange={handleChange}
            />
          </div>
          <div className="form-group">
            <label>Montant Dépensé</label>
            <input
              type="number"
              name="amount_spent"
              className="form-control"
              value={projectData.amount_spent || ""}
              onChange={handleChange}
            />
          </div>
          <div className="form-group d-flex justify-content-end">
            <button
              type="button"
              className="btn btn-secondary"
              onClick={handleClose}
            >
              Annuler
            </button>
            <button type="submit" className="btn btn-primary">
              Valider
            </button>
          </div>
        </form>
      </Modal.Body>
    </Modal>
  );
};

export default EditProjectModal;
