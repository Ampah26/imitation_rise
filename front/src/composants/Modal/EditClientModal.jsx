import React, { useState, useEffect } from "react";
import { Modal, Form } from "react-bootstrap";
import Swal from "sweetalert2";
import { countries } from "countries-list";
import Obligatoire from "../Obligatoire";

const EditClientModal = ({ show, handleClose, client }) => {
  const [clientData, setClientData] = useState(client || {});
  const [isOrganization, setIsOrganization] = useState(true);

  const countryOptions = Object.keys(countries).map((code) => ({
    value: code,
    label: countries[code].name,
  }));

  const currencies = [{ code: "EUR", name: "EURO Français", symbol: "€" }];

  useEffect(() => {
    if (client) {
      setClientData(client);
      setIsOrganization(client.type === "Organization");
    }
  }, [client]);

  const handleChange = (e) => {
    const { name, value } = e.target;

    if (name === "currency") {
      const selectedCurrency = currencies.find(
        (currency) => currency.code === value
      );
      setClientData((prevData) => ({
        ...prevData,
        [name]: value,
        currency_symbol: selectedCurrency ? selectedCurrency.symbol : "",
      }));
    } else {
      setClientData((prevData) => {
        const updatedData = { ...prevData, [name]: value };

        if (name === "total_invoiced" || name === "payment_received") {
          const totalInvoiced = parseFloat(updatedData.total_invoiced) || 0;
          const paymentReceived = parseFloat(updatedData.payment_received) || 0;
          const due = totalInvoiced - paymentReceived;
          updatedData.due = due.toFixed(2);
        }

        return updatedData;
      });
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const response = await fetch(
        `http://localhost:5000/api/clients/edit/${clientData.id}`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(clientData),
        }
      );

      if (response.ok) {
        const data = await response.json();
        Swal.fire({
          title: "Succès",
          text: "Client modifié avec succès !",
          icon: "success",
          confirmButtonText: "OK",
        });
        handleClose();
      } else {
        throw new Error("Erreur lors de la modification du client");
      }
    } catch (error) {
      console.error("Erreur:", error);
      Swal.fire({
        title: "Erreur",
        text: error.message,
        icon: "error",
        confirmButtonText: "OK",
      });
    }
  };

  return (
    <Modal show={show} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>Modifier le Client</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <form onSubmit={handleSubmit} className="general-form">
          <div className="form-group d-flex">
            <Form.Check
              type="radio"
              label="Organization"
              name="type"
              value="Organization"
              checked={isOrganization}
              onChange={(e) => {
                setIsOrganization(e.target.value === "Organization");
                handleChange(e);
              }}
              className="me-3"
            />
            <Form.Check
              type="radio"
              label="Individual"
              name="type"
              value="Individual"
              checked={!isOrganization}
              onChange={(e) => {
                setIsOrganization(e.target.value === "Organization");
                handleChange(e);
              }}
            />
          </div>

          {isOrganization ? (
            <>
              <div className="form-group">
                <input
                  type="text"
                  name="company_name"
                  className="form-control p10"
                  placeholder="Company Name"
                  value={clientData.company_name || ""}
                  onChange={handleChange}
                />
                <Obligatoire />
              </div>
              <div className="form-group">
                <input
                  type="text"
                  name="primary_contact"
                  className="form-control p10"
                  placeholder="Primary Contact Name"
                  value={clientData.primary_contact || ""}
                  onChange={handleChange}
                />
              </div>
            </>
          ) : (
            <div className="form-group">
              <input
                type="text"
                name="name"
                className="form-control p10"
                placeholder="Name"
                value={clientData.name || ""}
                onChange={handleChange}
              />
              <Obligatoire />
            </div>
          )}

          <div className="form-group">
            <label htmlFor="country">Pays</label>
            <select
              name="country"
              className="form-control p10"
              value={clientData.country || ""}
              onChange={handleChange}
            >
              <option value="" disabled>
                Sélectionnez un pays
              </option>
              {countryOptions.map((option) => (
                <option key={option.value} value={option.value}>
                  {option.label}
                </option>
              ))}
            </select>
          </div>

          <div className="form-group">
            <input
              type="tel"
              name="phone"
              className="form-control p10"
              placeholder="Phone"
              value={clientData.phone || ""}
              onChange={handleChange}
              required
            />
            <Obligatoire />
          </div>

          <div className="form-group">
            <label>Catégorie Client</label>
            <select
              name="client_group"
              className="form-control p10"
              value={clientData.client_group || ""}
              onChange={handleChange}
            >
              <option value="Gold">Gold</option>
              <option value="Silver">Silver</option>
              <option value="VIP">VIP</option>
            </select>
          </div>

          <div className="form-group">
            <label>Etiquette</label>
            <select
              name="labels"
              className="form-control p10"
              value={clientData.labels || ""}
              onChange={handleChange}
            >
              <option value="Satisfied">Satisfied</option>
              <option value="Active">Active</option>
              <option value="Unsatisfied">Unsatisfied</option>
              <option value="Inactive">Inactive</option>
            </select>
          </div>

          <div className="form-group">
            <label> Nombre de projet</label>
            <input
              type="number"
              name="projects"
              id="projects"
              className="form-control p10"
              placeholder="Projects"
              value={clientData.projects}
              onChange={handleChange}
            />
            <Obligatoire />
          </div>

          <div className="form-group">
            <label>Facture Total</label>
            <input
              type="number"
              name="total_invoiced"
              id="total_invoiced"
              className="form-control p10"
              placeholder="Total Invoiced"
              value={clientData.total_invoiced}
              onChange={handleChange}
            />
            <Obligatoire />
          </div>

          <div className="form-group">
            <label>Payement reçu</label>
            <input
              type="number"
              name="payment_received"
              id="payment_received"
              className="form-control p10"
              placeholder="Payment Received"
              value={clientData.payment_received}
              onChange={handleChange}
            />
          </div>

          <div className="form-group">
            <label>Montant restant</label>
            <input
              type="number"
              name="due"
              id="due"
              className="form-control p10"
              placeholder="Due"
              value={clientData.due}
              onChange={handleChange}
            />
          </div>

          <div className="form-group">
            <label> Adresse </label>
            <input
              type="text"
              name="address"
              id="address"
              className="form-control p10"
              placeholder="Address"
              value={clientData.address}
              onChange={handleChange}
            />
          </div>

          <div className="form-group">
            <label>Ville</label>
            <input
              type="text"
              name="city"
              id="city"
              className="form-control p10"
              placeholder="City"
              value={clientData.city}
              onChange={handleChange}
            />
          </div>

          <div className="form-group">
            <input
              type="text"
              name="state"
              id="state"
              className="form-control p10"
              placeholder="State"
              value={clientData.state}
              onChange={handleChange}
            />
          </div>

          <div className="form-group">
            <input
              type="text"
              name="zip"
              id="zip"
              className="form-control p10"
              placeholder="Zip"
              value={clientData.zip}
              onChange={handleChange}
            />
          </div>

          <div className="form-group">
            <input
              type="text"
              name="website"
              id="website"
              className="form-control p10"
              placeholder="Website"
              value={clientData.website}
              onChange={handleChange}
            />
          </div>

          <div className="form-group">
            <input
              type="text"
              name="vat_number"
              id="vat_number"
              className="form-control p10"
              placeholder="VAT Number"
              value={clientData.vat_number}
              onChange={handleChange}
            />
          </div>

          <div className="form-group">
            <input
              type="text"
              name="fiscal_identification"
              id="fiscal_identification"
              className="form-control p10"
              placeholder="Fiscal Identification"
              value={clientData.fiscal_identification}
              onChange={handleChange}
            />
          </div>

          <div className="form-group d-flex justify-content-between">
            <button
              type="button"
              className="btn btn-secondary"
              onClick={handleClose}
            >
              Annuler
            </button>
            <button type="submit" className="btn btn-primary">
              Enregistrer les modifications
            </button>
          </div>
        </form>
      </Modal.Body>
    </Modal>
  );
};

export default EditClientModal;
