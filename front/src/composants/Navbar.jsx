import React from "react";
import { Link, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";

const Navbar = ({ toggleSidebar }) => {
  const navigate = useNavigate();

  const nom = localStorage.getItem("nom");
  const prenom = localStorage.getItem("prenom");

  const handleLogout = () => {
    Swal.fire({
      icon: "question",
      title: "Êtes-vous sûr ?",
      text: "Voulez-vous vraiment vous déconnecter ?",
      showCancelButton: true,
      confirmButtonText: "Oui, déconnecter !",
      cancelButtonText: "Non, annuler",
    }).then((result) => {
      if (result.isConfirmed) {
        // Supprimer les informations de connexion
        localStorage.removeItem("token");
        localStorage.removeItem("nom");
        localStorage.removeItem("prenom");

        // Rediriger vers la page de connexion
        navigate("/");
      }
    });
  };

  return (
    <div>
      <nav
        className="navbar navbar-expand fixed-top navbar-light navbar-custom shadow-sm"
        role="navigation"
      >
        <div className="container-fluid">
          {/* Ajout du logo */}
          <Link className="navbar-brand">
            <img
              src="/promanageLogo.png"
              alt="Promanage Logo"
              style={{ height: "60px" }}
            />
          </Link>

          <div className="collapse navbar-collapse">
            <div className="d-flex w-auto"></div>
            <div className="navbar-nav me-auto mb-lg-0"></div>

            <div className="d-flex w-auto">
              <ul className="navbar-nav">
                <li className="nav-item hidden-sm" title="Search (/)">
                  <ul
                    className="nav-link"
                    data-modal-title="Search (/)"
                    data-post-hide-header="1"
                    data-modal-close="1"
                    id="global-search-btn"
                    data-act="ajax-modal"
                    data-title="Search (/)"
                    data-action-url="https://rise.fairsketch.com/search/search_modal_form"
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      stroke="currentColor"
                      strokeWidth="2"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      className="feather feather-search icon"
                    >
                      <circle cx="11" cy="11" r="8"></circle>
                      <line x1="21" y1="21" x2="16.65" y2="16.65"></line>
                    </svg>
                  </ul>
                </li>

                <li className="nav-item dropdown">
                  <ul
                    id="web-notification-icon"
                    className="nav-link dropdown-toggle"
                    data-bs-toggle="dropdown"
                    data-total="3"
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      stroke="currentColor"
                      strokeWidth="2"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      className="feather feather-bell icon"
                    >
                      <path d="M18 8A6 6 0 0 0 6 8c0 7-3 9-3 9h18s-3-2-3-9"></path>
                      <path d="M13.73 21a2 2 0 0 1-3.46 0"></path>
                    </svg>{" "}
                    <span className="badge bg-danger up">3</span>
                  </ul>
                  <div className="dropdown-menu dropdown-menu-end notification-dropdown w400">
                    <div className="dropdown-details card bg-white m0">
                      <div className="list-group">
                        <span className="list-group-item inline-loader p10"></span>
                      </div>
                    </div>
                  </div>
                </li>
                <li className="nav-item dropdown">
                  <ul
                    id="user-dropdown"
                    className="nav-link dropdown-toggle"
                    data-bs-toggle="dropdown"
                    role="button"
                    aria-expanded="false"
                  >
                    <span className="avatar-xs avatar me-1">
                      <img
                        alt="..."
                        src="https://rise.fairsketch.com/files/profile_images/_file62ad94f892365-avatar.png"
                      />
                    </span>
                    {nom && prenom ? (
                      <span className="user-name ml10">
                        Bienvenue, {prenom} {nom}
                      </span>
                    ) : (
                      <span className="user-name ml10">Utilisateur</span>
                    )}
                  </ul>
                  <ul className="dropdown-menu dropdown-menu-end w200 user-dropdown-menu">
                    {nom && prenom ? (
                      <>
                        <li>
                          <Link className="dropdown-item">
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              width="24"
                              height="24"
                              viewBox="0 0 24 24"
                              fill="none"
                              stroke="currentColor"
                              strokeWidth="2"
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              className="feather feather-user icon-16 me-2"
                            >
                              <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
                              <circle cx="12" cy="7" r="4"></circle>
                            </svg>{" "}
                            Mon profil
                          </Link>
                        </li>
                        <li>
                          <Link className="dropdown-item">
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              width="24"
                              height="24"
                              viewBox="0 0 24 24"
                              fill="none"
                              stroke="currentColor"
                              strokeWidth="2"
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              className="feather feather-key icon-16 me-2"
                            >
                              <path d="M21 2l-2 2m-7.61 7.61a5.5 5.5 0 1 1-7.778 7.778 5.5 5.5 0 0 1 7.777-7.777zm0 0L15.5 7.5m0 0l3 3L22 7l-3-3m-3.5 3.5L19 4"></path>
                            </svg>{" "}
                            Modifier mot de passe
                          </Link>
                        </li>

                        <li className="dropdown-divider"></li>
                        <li>
                          <Link
                            className="dropdown-item"
                            onClick={handleLogout}
                          >
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              width="24"
                              height="24"
                              viewBox="0 0 24 24"
                              fill="none"
                              stroke="currentColor"
                              strokeWidth="2"
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              className="feather feather-log-out icon-16 me-2"
                            >
                              <path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path>
                              <polyline points="16 17 21 12 16 7"></polyline>
                              <line x1="21" y1="12" x2="9" y2="12"></line>
                            </svg>{" "}
                            Deconnexion
                          </Link>
                        </li>
                      </>
                    ) : (
                      <li className="dropdown-item">
                        <button
                          className="btn btn-primary w-100"
                          onClick={() => (window.location.href = "/")}
                        >
                          Veuillez vous connecter
                        </button>
                      </li>
                    )}
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </nav>
    </div>
  );
};

export default Navbar;
