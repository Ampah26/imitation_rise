import React from "react";

const Footer = () => {
  return (
    <div className="footer">
      <div className="float-start">Copyright © Ampah KOMLASSAN</div>
    </div>
  );
};

export default Footer;
