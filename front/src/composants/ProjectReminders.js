import React, { useEffect, useState } from "react";
import axios from "axios";

const ProjectReminders = () => {
  const [reminders, setReminders] = useState({ delayed: [], upcoming: [] });
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    // Récupérer les données depuis le backend
    axios
      .get("http://localhost:5000/api/projects/reminder")
      .then((response) => {
        setReminders(response.data);
        setLoading(false);
      })
      .catch((error) => {
        console.error("Erreur lors du chargement des rappels:", error);
        setLoading(false);
      });
  }, []);

  if (loading) return <p>Chargement des données...</p>;

  return (
    <div className="project-reminders">
      <h3>Rappels Projets</h3>

      {/* Projets en retard */}
      <div>
        <h4>Projets en Retard</h4>
        {reminders.delayed.length > 0 ? (
          <ul>
            {reminders.delayed.map((project) => (
              <li key={project.id}>
                <strong>{project.name}</strong> - Échéance dépassée le :{" "}
                {new Date(project.end_date).toLocaleDateString()}
              </li>
            ))}
          </ul>
        ) : (
          <p>Pas de projets en retard 🎉</p>
        )}
      </div>

      {/* Projets à venir */}
      <div>
        <h4>Projets à Échéance Proche</h4>
        {reminders.upcoming.length > 0 ? (
          <ul>
            {reminders.upcoming.map((project) => (
              <li key={project.id}>
                <strong>{project.name}</strong> - Échéance :{" "}
                {new Date(project.end_date).toLocaleDateString()}
              </li>
            ))}
          </ul>
        ) : (
          <p>Pas de projets avec des échéances proches 👍</p>
        )}
      </div>
    </div>
  );
};

export default ProjectReminders;
