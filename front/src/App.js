import React from "react";
import "./App.css";
import Login from "./composants/Login";
import Register from "./composants/Register";
import Dashboard from "./composants/Admin/Dashboard";
import Clients from "./composants/Admin/Clients";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Projects from "./composants/Admin/Projects";
import Tasks from "./composants/Admin/Tasks";
import Team from "./composants/Admin/Team";
import Messages from "./composants/Admin/Messages";
import Expenses from "./composants/Admin/Expenses";
import "bootstrap/dist/css/bootstrap.min.css";
import Footer from "./composants/Footer";

function App() {
  return (
    <div className="app">
      <Router>
        <Routes>
          <Route path="/" element={<Login />} />
          <Route path="/admin.dashboard" element={<Dashboard />} />
          <Route path="/admin.clients" element={<Clients />} />
          <Route path="/admin.projects" element={<Projects />} />
          <Route path="/admin.tasks" element={<Tasks />} />
          <Route path="/admin.messages" element={<Messages />} />
          <Route path="/admin.expenses" element={<Expenses />} />
          <Route path="/admin.team" element={<Team />} />

          <Route path="/signup" element={<Register />} />
        </Routes>
      </Router>
      <Footer />
    </div>
  );
}

export default App;
